package com.classpath.oauth.controller;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class OAuthController {
	
	@GetMapping
	public Map<String, Object> extractUserDetails(OAuth2Authentication oauth2) {
		Object principal = oauth2.getPrincipal();
		Collection<? extends GrantedAuthority> authorities = oauth2.getUserAuthentication().getAuthorities();
		Set<String> authoritiesSet = AuthorityUtils.authorityListToSet(authorities);
		Map<String, Object> userDetails = new HashMap<>();
		userDetails.put("username", principal);
		userDetails.put("authorities", authoritiesSet);
		return userDetails;
	}
}